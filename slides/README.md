# Developer Survival Guide - A Presentation

Install yarn:

https://yarnpkg.com/lang/en/docs/install/

## Project setup

```
yarn install
```

### Run the presentation

```
yarn run serve
```

Open browser to http://localhost:8080 and profit. Use the arrow keys to navigate, or press ESC to page through the slides.
