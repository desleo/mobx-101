import Title from "./Title.vue";
import AboutMe from "./AboutMe.vue";
import StateManagement from "./StateManagement.vue";
import MobxBasics from "./MobxBasics.vue";
import ThankYou from "./ThankYou.vue";

export default {
  Title,
  AboutMe,
  StateManagement,
  MobxBasics,
  ThankYou,
};
