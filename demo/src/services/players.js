class PlayerService {
  static getPlayers() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
          resolve([
            { name: 'Leo', level: 10, initiative: 23 },
            { name: 'Steve', level: 9, initiative: 4 },
            { name: 'Nick', level: 10, initiative: 7 },
            { name: 'Mavis', level: 11, initiative: 12 },
            { name: 'Mike', level: 8, initiative: 16 },                        
          ]);
      }, 4000);
    });
  }

  static updatePlayers() {
    console.log('Some server operation!');

    return new Promise((resolve) => {
      setTimeout(resolve, 2000);
    }); 
  }
}

export default PlayerService;