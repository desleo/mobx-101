import { observable, computed, action, flow, runInAction, when, autorun } from 'mobx' 
import PlayerService from '../services/players';
import Player from '../models/player';

class InitiativeStore {
  @observable players = [];
  @observable round = 1;
  @observable initialized = false;
  @observable isLoading = false;  

  @computed get isReady() {    
    return this.initialized 
      && !this.isLoading;
  }

  @computed get pendingPlayers() {    
    return this.players
      .filter((player) => !player.hasActed)
      .sort(this.sortDescByInit);
  }

  @computed get nextPlayer() {
    return this.pendingPlayers[0];
  }

  @action.bound
  addPlayer(name, level, initiative) {
    this.players.push(new Player(name, level, initiative));
  }

  @action.bound
  async loadPlayers() {
    this.isLoading = true;

    try {
      const players = await PlayerService.getPlayers();
      runInAction(() => {
        this.players = players.map(({name, level, initiative}) => 
          new Player(name, level, initiative)
        ); 
      });      
    } catch(error) {
      console.log('Dead code. We never have errors.');
    } finally {
      runInAction(() => {
        this.isLoading = false;
      });      
    }
  };

  betterLoadPlayers = flow(function* () {
    this.isLoading = true;
  
    try {
      const players = yield PlayerService.getPlayers();
      this.players = players.map(({name, level, initiative}) => 
        new Player(name, level, initiative)
      ); 
    } catch(error) {
      console.log('Dead code. We never have errors.');
    } finally {
      this.isLoading = false;
    }
  });

  constructor() {
    when(() => this.players.length)
      .then(() => {
        this.initialized = true;
        console.log('initialized.');
      });  

    autorun(() => {
      console.log('Player count changed', this.players.length);
      PlayerService.updatePlayers(this.players);
    }, { delay: 500 });

    autorun(() => {      
      if (this.initialized && this.pendingPlayers.length === 0) {
        this.players.forEach((player) => player.hasActed = false);
        this.round = this.round + 1;
      }
    });
  }

  sortDescByInit(leftPlayer, rightPlayer) {
    return rightPlayer.initiative - leftPlayer.initiative;
  }
}

export default InitiativeStore;