import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { Provider } from 'mobx-react';
import Basics from './pages/basics';
import Hooks from './pages/hooks';
import Stores from './pages/stores';
import logo from './logo.svg';
import './App.scss';
import InitiativeStore from './stores/initiative';

function App() {
  return (
    <Router>
      <div className="App">
        <nav>
          <ul>
            <li>
              <Link to="/store">Store</Link>
            </li>
          </ul>
        </nav>
        <Provider initiativeStore={new InitiativeStore()}>
          <Switch>
            <Route path="/basic">
              <Basics />
            </Route>
            <Route path="/store">            
              <Stores />            
            </Route>
            <Route path="/hooks">
              <Hooks />
            </Route>
            <Route path="/">
              <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                  Mobx 101
                </p>
              </header>
            </Route>
          </Switch>
        </Provider>            
      </div>
    </Router>
  );
}

export default App;
