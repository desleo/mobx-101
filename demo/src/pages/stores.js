import React from 'react';
import { observer, inject } from 'mobx-react';
import NewPlayerForm from '../components/newPlayerForm';
import RoundParticipant from '../components/roundParticipant';

class Stores extends React.Component {
  componentDidMount() {
    const { store } = this.props;
    store.betterLoadPlayers();    
  }

  onAdvanceRound = () => {
    const { store } = this.props;

    if (store.nextPlayer) {
      store.nextPlayer.toggleActed();
    }
  }

  render() {
    const { store } = this.props;
    
    if (!store.isReady) {
      return (<p>Loading ...</p>);
    }

    return (
      <div className="initiative-container">
        <div className="actions">
          <h1>Round: {store.round}</h1>          
          <NewPlayerForm addPlayer={store.addPlayer} />
        </div>
        <div className="list">
          <button type="button" onClick={this.onAdvanceRound}>
            Advance Round
          </button>
          {store.pendingPlayers.map((player, index) => (
            <RoundParticipant key={index} player={player} />            
          ))}
        </div>
      </div>
    );  
  }
}

export {
  Stores,
};

export default inject((stores) => ({
  store: stores.initiativeStore,
}))(
  observer(Stores)
);