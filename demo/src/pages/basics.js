import React from 'react';
import { observer } from 'mobx-react';
import Player from '../models/player';

class Basics extends React.Component {
  constructor(props) {
    super(props);
    this.player = new Player('Leo', 11);
  }

  increase = () => {
    this.player.setInitiative(this.player.initiative + 1);
  }

  decrease= () => {
    this.player.setInitiative(this.player.initiative - 1);
  }

  render() {
    return (
      <div>
        <h1>{this.player.displayName}</h1>
        <p>
          Initiative: {this.player.initiative}
        </p>
        <button type="button" onClick={this.increase}>Increase</button>
        <button type="button" onClick={this.decrease}>Decrease</button>
      </div>
    );  
  }
}

export {
  Basics,
};

export default observer(Basics);