import React, { useEffect } from 'react';
import { observer } from 'mobx-react';
import useInitStore from '../hooks/useInitStore';
import NewPlayerForm from '../components/newPlayerForm';
import RoundParticipant from '../components/roundParticipant';

export function Hooks() {
  const store = useInitStore();
  
  useEffect(() => {
    store.betterLoadPlayers();
  }, [store]);
  
  const onAdvanceRound = () => {
    if (store.nextPlayer) {
      store.nextPlayer.toggleActed();
    }
  };

  if (!store.isReady) {
    return (<p>Loading ...</p>);
  }

  return (
    <div className="initiative-container">
      <NewPlayerForm addPlayer={store.addPlayer} />
      <div>
        <p>Round: {store.round}</p>
        
        <button type="button" onClick={onAdvanceRound}>
          Next
        </button>
      </div>        
      <div>
        {store.pendingPlayers.map((player, index) => (
          <RoundParticipant key={index} player={player} />            
        ))}
      </div>
    </div>
  );  
}

export default observer(Hooks);
