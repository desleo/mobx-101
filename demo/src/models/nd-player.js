import { observable, computed, action, decorate } from "mobx"

class Player {
    name;
    level;

    constructor(name, level) {
        this.name = name;
        this.level = level;
    }

    get displayName() {
        return `${level} - ${name}`;
    }

    setInitiative(newInit) {
        this.initiative = newInit;
    }
}
decorate(Player, {
    name: observable,
    level: observable,
    displayName: computed,
    setInitiative: action
});

export default Player;