import { observable, computed, action } from 'mobx'

class Player {
    @observable name;
    @observable level;
    @observable initiative;
    @observable hasActed = false;

    constructor(name = '', level = 0, initiative = 0) {
      this.name = name;
      this.level = level;
      this.initiative = initiative;
    }

    @computed get displayName() {
      return `${this.name}: [Lvl ${this.level}]`;
    }

    @action.bound
    setInitiative(newInit) {
      this.initiative = newInit;
    }

    @action.bound
    toggleActed() {
      this.hasActed = !this.hasActed;
    }
}

export default Player;
