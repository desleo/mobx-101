import React from 'react';
import { MobXProviderContext } from 'mobx-react'

export default function useInitStore() {
  const context = React.useContext(MobXProviderContext);
  return context.initiativeStore;
}