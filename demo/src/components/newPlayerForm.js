import React, { useState } from 'react';

export default function NewPlayerForm({ addPlayer }) {
  const [ name, setName] = useState('');
  const [ level, setLevel] = useState(0);
  const [ initiative, setInitiative] = useState(0);

  const submit = (event) => {
    event.preventDefault();
    addPlayer(name, level, initiative)
  };
  
  return (
    <div className="new-player-container">      
      <form onSubmit={submit}>                        
        <label>
          <span>Name:</span>
          <input 
            type="text" 
            value={name} 
            onChange={({target}) => setName(target.value)}
          />
        </label>

        <div className="box">
          <label>
            <span>Level:</span>
            <input 
              type="number" 
              value={level} 
              onChange={({target}) => setLevel(Number(target.value))}
            />
          </label>
          
          <label>
            <span>Init:</span>
            <input 
              type="number"
              value={initiative}
              onChange={({target}) => setInitiative(Number(target.value))}
            />
          </label>     
        </div>
        <button type="submit">
          Add New Player
        </button>    
      </form> 
    </div>
  );
}