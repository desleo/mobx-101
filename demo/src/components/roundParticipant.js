import React from 'react';

export default function RoundParticipant({ player }) {
  return (
    <div className="player-container">
      <p>Init: {player.initiative}</p>
      <p>{player.displayName}</p>
    </div>
  );
}