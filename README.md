# MobX 101 - A Presentation

## Project setup (in the slides and/or demo folder)

```
npm install
```

### Run the presentation from the slides folder with

```
npm run serve
```

Open browser to http://localhost:8080 and profit for the slides or port 3000 for the demo. Use the arrow keys to navigate, or press ESC to page through the slides.

### Run the demo from the demo folder with

```
npm run start
```

The demo should open to http://localhost:3000
